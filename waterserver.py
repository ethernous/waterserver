import requests
import json
import datetime as dt
from time import time, sleep
import pytz
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="wavelmon"
)

mycursor = mydb.cursor()

while True:
    sleep(300 - time() % 300)
	# thing to run

    url = "http://data.luweswatersensor.com:8002/mitratel"

    payload='a=stat&imei=866191037511318'
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    hasil = response.text

    data = json.loads(hasil)

    lokasi = data["name"]

    tinggi = data["level_sensor"]

    tegangan = data["power_voltage"]

    arus = data["power_current"]

    latitude = data["accel_x"]

    longitude = data["accel_y"]

    utc_now = pytz.utc.localize(dt.datetime.utcnow())
    pst_now = utc_now.astimezone(pytz.timezone("Asia/Jakarta"))
    timestamp = pst_now.strftime("%Y-%m-%d %H:%M:%S")
    waktu = utc_now.strftime("%Y-%m-%d %H:%M:%S")

    sql = "INSERT INTO monitoringdata (id_data, namadevice, level_sensor, power_voltage, power_current, latitude, longitude, waktu, created_at, updated_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = ('NULL', lokasi, tinggi, tegangan, arus, latitude, longitude, waktu, timestamp, timestamp)
    mycursor.execute(sql,val)

    mydb.commit()

    sql2 = "UPDATE monitoring SET updated_at = %s WHERE namamonitoring = %s"
    val2 = timestamp, lokasi
    mycursor.execute(sql2,val2)

    mydb.commit()


    print(mycursor.rowcount, "record inserted.")
    print(timestamp)
    print(waktu)
